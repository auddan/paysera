var gulp = require('gulp');
var eslint = require('gulp-eslint');
var mocha = require('gulp-mocha');

var files = {
    src: 'src/**/*.js',
    test: 'src/**/*.spec.js'
};

gulp.task('lint', () => {
    return gulp.src([files.src])
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});

gulp.task('unit-tests', () =>
    gulp.src(files.test, {read: false})
        .pipe(mocha())
);

gulp.task('default', ['lint', 'unit-tests'], function() {
    gulp.watch([files.test], ['unit-tests']);
});